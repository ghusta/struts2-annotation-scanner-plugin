<#assign fa_underscore>
<i class="fa fa-window-minimize" aria-hidden="true"></i><#t>
</#assign>
<#assign classIconOpened = "fa-chevron-down" >
<#assign classIconClosed = "fa-chevron-right" >
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Annotations Scan Report</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
    .card-header.clickable {
        cursor: pointer;
    }
    .icon-title {
        padding: 0.2em 0.25em 0.15em;
        margin-right: 0.15em;
    }
    </style>

    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

    <#-- Init all Tooltips (cf. https://getbootstrap.com/docs/4.6/components/tooltips/) -->
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });

            $('.collapse')
                .on('show.bs.collapse', function(){
                    $(this).parent().find(".${classIconClosed}").removeClass("${classIconClosed}").addClass("${classIconOpened}");
                })
                .on('hide.bs.collapse', function(){
                    $(this).parent().find(".${classIconOpened}").removeClass("${classIconOpened}").addClass("${classIconClosed}");
                });

            $("input[name='opts_width']").change(function(){
                if ($(this).val() === 'normal') {
                    $('#main-container').removeClass("container-fluid").addClass("container");
                }
                else if ($(this).val() === 'full') {
                    $('#main-container').removeClass("container").addClass("container-fluid");
                }
            });

        });
    </script>

</head>
<body>
<div id="main-container" class="container">
    <h1>
        <span class="fa-stack">
            <i class="fa fa-circle fa-stack-2x text-primary"></i>
            <i class="fa fa-search fa-flip-horizontal fa-stack-1x fa-inverse"></i>
        </span>
        Annotations Scan Report
    </h1>

    <div class="row">
        <div class="col">
            <h2>Actions list by namespaces</h2>
        </div>
        <div class="col">
            <div class="btn-group btn-group-toggle float-right" data-toggle="buttons">
                <label class="btn btn-outline-secondary active">
                    <input type="radio" name="opts_width" value="normal" autocomplete="off" checked><i class="fa fa-compress fa-fw" aria-hidden="true"></i> Fixed-width
                </label>
                <label class="btn btn-outline-secondary">
                    <input type="radio" name="opts_width" value="full" autocomplete="off"><i class="fa fa-expand fa-fw" aria-hidden="true"></i> Full-width
                </label>
            </div>
        </div>
    </div>

<#-- https://getbootstrap.com/docs/4.6/components/collapse/#accordion-example -->
<#-- https://getbootstrap.com/docs/4.6/components/list-group/#with-badges -->
    <div class="accordion" id="accordionExample">
    <#list namespaces as ns>
        <!-- Namespace #${ns?counter} -->
        <#assign idx = ns?counter >
        <#assign actionCount = actionNamesByNamespace[ns]?size >
        <div class="card border-primary">
            <a href="#" class="card-header" id="heading-${idx}" data-toggle="collapse" data-target="#collapse-${idx}" aria-expanded="true" aria-controls="collapse-${idx}">
                <h5 class="mb-0">
                    <i class="fa fa-fw <#if idx == 1>${classIconOpened}<#else>${classIconClosed}</#if>" aria-hidden="true"></i>
                    <span>"${ns}"</span>
                    <span class="badge badge-secondary float-right">${actionCount}</span>
                </h5>
            </a>
            <div id="collapse-${idx}" class="collapse <#if idx == 1>show</#if>" aria-labelledby="heading-${idx}" data-parent="#accordionExample">
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Action Name</th>
                                <th>Action Class</th>
                                <th>Action Method</th>
                                <th>Annotations (on method)</th>
                            </tr>
                        </thead>
                        <tbody>
                        <#list actionConfigDataMap[ns] as actionName, actionConfigData>
                            <tr class="<#if (actionConfigData.actionClassNotFound) >table-danger<#else><#if (actionConfigData.actionInitializationError) >table-warning</#if></#if>">
                                <td>"${actionName}"</td>
                                <td class="small" style="max-width: 400px;">
                                    <#if (actionConfigData.resolvedActionClass)?? && actionConfigData.resolvedActionClass.name != actionConfigData.className>
                                        <#assign actionClassWithWordBreaks = actionConfigData.resolvedActionClass.name.replace(".", ".<wbr/>") />
                                    <span style="word-wrap: break-word;">${actionClassWithWordBreaks!"<!-- empty -->"}</span>
                                        <#assign tooltip_msg = "Action Class was resolved <br/>from <code>" + actionConfigData.className + "</code>" />
                                    <i class="fa fa-info-circle fa-fw text-info" style="cursor: help;" aria-hidden="true" data-toggle="tooltip" data-placement="auto" data-html="true" title="${tooltip_msg}"></i>
                                    <#else>
                                        <#-- Original class name (unresolved) -->
                                        <#assign actionClassWithWordBreaks = actionConfigData.className.replace(".", ".<wbr/>") />
                                    <span style="word-wrap: break-word;">${(actionClassWithWordBreaks)!"<!-- empty -->"}</span>
                                        <#if (actionConfigData.actionClassNotFound) >
                                    <i class="fa fa-exclamation-triangle fa-fw text-danger" style="cursor: help;" aria-hidden="true" data-toggle="tooltip" data-placement="auto" data-html="true" title="Action Class not resolved"></i>
                                        </#if>
                                        <#if actionConfigData.actionInitializationError == true>
                                            <#assign tooltip_init_error_msg = "Error while initializing action <br/><code>" + actionConfigData.actionInitializationErrorMessage + "</code>" />
                                    <i class="fa fa-exclamation-triangle fa-fw text-warning" style="cursor: help;" aria-hidden="true" data-toggle="tooltip" data-placement="auto" data-html="true" title="${tooltip_init_error_msg}"></i>
                                        </#if>
                                    </#if>
                                </td>
                                <td>${actionConfigData.methodName!"<!-- empty -->"}</td>
                                <td>
                                    <ul>
                                    <#if (actionConfigData.methodAnnotations)?? >
                                        <#list actionConfigData.methodAnnotations as methodAnnot>
                                        <li><samp>${action.annotationToHtmlWithWbr(methodAnnot)}</samp></li>
                                        </#list>
                                    </#if>
                                    </ul>
                                </td>
                            </tr>
                        </#list>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </#list>
    </div>

    <br/>

    <!-- FOOTER -->
    <div>
    </div>
</div>
<!-- Generated by Freemarker v${.version} -->
</body>
</html>