<#assign fa_underscore>
<i class="fa fa-window-minimize" aria-hidden="true"></i><#t>
</#assign>
<!DOCTYPE html>
<html>
<head>
    <title>Scan Report</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<div id="main-container" class="container">
    <h1>Scan Report</h1>

    <#-- ROW 1 -->
    <div class="row">
        <div class="col">
            <div class="card border-primary">
                <label class="card-header">Namespaces List</label>
                <ul class="list-group list-group-flush text-primary">
                    <#list namespaces as ns>
                    <#assign actionCount = actionNamesByNamespace[ns]?size >
                    <!-- "${ns}" contains ${actionCount} action(s) -->
                    <#if ns?trim == "">
                    <li class="list-group-item">${fa_underscore}</li>
                    <#else>
                    <li class="list-group-item">${ns}</li>
                    </#if>
                    </#list>
                </ul>
                <div class="card-footer">
                    <small class="text-muted">Total namespaces : ${namespaces?size}</small>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <div>
        <span>Freemarker version : <em>${.version}</em></span>
    </div>
</div>
</body>
</html>