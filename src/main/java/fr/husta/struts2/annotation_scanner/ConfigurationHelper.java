/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package fr.husta.struts2.annotation_scanner;

import com.opensymphony.xwork2.config.Configuration;
import com.opensymphony.xwork2.config.entities.ActionConfig;
import com.opensymphony.xwork2.config.entities.PackageConfig;
import com.opensymphony.xwork2.inject.Inject;
import com.opensymphony.xwork2.util.ResolverUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * ConfigurationHelper
 */
public class ConfigurationHelper {

    private Configuration configuration;

    @Inject
    public void setConfiguration(Configuration config) {
        this.configuration = config;
    }

    public Set<String> getNamespaces() {
        Set<String> namespaces = Collections.emptySet();
        Map<String, Map<String, ActionConfig>> allActionConfigs = configuration.getRuntimeConfiguration().getActionConfigs();
        if (allActionConfigs != null) {
            namespaces = allActionConfigs.keySet();
        }
        return namespaces;
    }

    public Map<String, PackageConfig> getPackageConfigs() {
        return configuration.getPackageConfigs();
    }

    public Set<String> getActionNames(String namespace) {
        Set<String> actionNames = Collections.emptySet();
        Map<String, Map<String, ActionConfig>> allActionConfigs = configuration.getRuntimeConfiguration().getActionConfigs();
        if (allActionConfigs != null) {
            Map<String, ActionConfig> actionMappings = allActionConfigs.get(namespace);
            if (actionMappings != null) {
                actionNames = actionMappings.keySet();
            }
        }
        return actionNames;
    }

    public Map<String, Map<String, ActionConfig>> getActionConfigs() {
        return configuration.getRuntimeConfiguration().getActionConfigs();
    }

    public ActionConfig getActionConfig(String namespace, String actionName) {
        ActionConfig config = null;
        Map<String, Map<String, ActionConfig>> allActionConfigs = configuration.getRuntimeConfiguration().getActionConfigs();
        if (allActionConfigs != null) {
            Map<String, ActionConfig> actionMappings = allActionConfigs.get(namespace);
            if (actionMappings != null) {
                config = actionMappings.get(actionName);
            }
        }
        return config;
    }

    @SuppressWarnings("unchecked")
    public List<Properties> getJarProperties() throws IOException {
        ResolverUtil resolver = new ResolverUtil();
        List<Properties> poms = new ArrayList<>();
        resolver.findNamedResource("pom.properties", "META-INF/maven");
        Set<URL> urls = resolver.getResources();
        for (URL url : urls) {
            Properties p = new Properties();
            try (InputStream openStream = url.openStream()) {
                p.load(openStream);
            }
            poms.add(p);
        }
        return poms;
    }
}