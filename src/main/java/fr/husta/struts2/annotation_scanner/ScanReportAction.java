package fr.husta.struts2.annotation_scanner;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ObjectFactory;
import com.opensymphony.xwork2.config.Configuration;
import com.opensymphony.xwork2.config.entities.ActionConfig;
import com.opensymphony.xwork2.inject.Inject;
import fr.husta.struts2.annotation_scanner.config.PluginConfiguration;
import fr.husta.struts2.annotation_scanner.filter.Filter;
import fr.husta.struts2.annotation_scanner.filter.MultipleRegexPatternFilter;
import fr.husta.struts2.annotation_scanner.utils.HtmlUtils;
import fr.husta.struts2.annotation_scanner.utils.ScanReportUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.StrutsConstants;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class ScanReportAction extends ActionSupport {

    private static final Logger logger = LogManager.getLogger();

    private ConfigurationHelper configHelper;

    private Configuration configuration;

    private ObjectFactory objectFactory;

    private PluginConfiguration pluginConfiguration;
    private Filter annotationFilter;

    @SuppressWarnings("unused")
    private String extension;

    @SuppressWarnings("unused")
    private boolean devMode = false;

    private Set<String> namespaces;
    private Map<String, Set<String>> actionNamesByNamespace;
    private Map<String, Map<String, ActionConfigData>> actionConfigDataMap;

    @Inject
    public void setConfigurationHelper(ConfigurationHelper cfg) {
        this.configHelper = cfg;
    }

    @Inject
    public void setConfiguration(Configuration config) {
        this.configuration = config;
    }

    /**
     * Inject an {@link ObjectFactory}.
     * See also the Struts constant {@link StrutsConstants#STRUTS_OBJECTFACTORY}.
     *
     * @param objectFactory
     */
    @Inject
    public void setObjectFactory(ObjectFactory objectFactory) {
        this.objectFactory = objectFactory;
        if (logger.isDebugEnabled()) {
            if (this.objectFactory != null) {
                logger.debug("Struts ObjectFactory (constant '{}') found : {}",
                        StrutsConstants.STRUTS_OBJECTFACTORY,
                        this.objectFactory.getClass().getName());
            } else {
                logger.debug("Struts ObjectFactory (constant '{}') not found",
                        StrutsConstants.STRUTS_OBJECTFACTORY);
            }
        }
    }

    @Inject
    public void setPluginConfiguration(PluginConfiguration pluginConfiguration) {
        this.pluginConfiguration = pluginConfiguration;

        this.annotationFilter
                = new MultipleRegexPatternFilter(this.pluginConfiguration.getIncludedAnnotationsPatterns());
    }

    @Inject(StrutsConstants.STRUTS_ACTION_EXTENSION)
    public void setExtension(String ext) {
        this.extension = ext;
    }

    @Inject(value = StrutsConstants.STRUTS_DEVMODE, required = false)
    public void setDevMode(String devMode) {
        this.devMode = Boolean.parseBoolean(devMode);
    }

    public SortedSet<String> getNamespaces() {
        return (SortedSet<String>) namespaces;
    }

    public Map<String, Set<String>> getActionNamesByNamespace() {
        return actionNamesByNamespace;
    }

    public Map<String, Map<String, ActionConfigData>> getActionConfigDataMap() {
        return actionConfigDataMap;
    }

    @Override
    public String execute() {
        namespaces = new TreeSet<>(configHelper.getNamespaces());
        if (namespaces.isEmpty()) {
            addActionError("There are no namespaces in this configuration");
            return ERROR;
        }

        actionNamesByNamespace = new HashMap<>();
        for (String ns : namespaces) {
            Set<String> actionNames = configHelper.getActionNames(ns);
            actionNamesByNamespace.put(ns, new TreeSet<>(actionNames));
        }

        actionConfigDataMap = new HashMap<>();
        Map<String, Map<String, ActionConfig>> allActionConfigs = configuration.getRuntimeConfiguration().getActionConfigs();
        for (Map.Entry<String, Map<String, ActionConfig>> entry : allActionConfigs.entrySet()) {
            String ns = entry.getKey();
            SortedMap<String, ActionConfigData> tempStrActionConfigDataMap = new TreeMap<>();
            for (String actionName : entry.getValue().keySet()) {
                ActionConfig actionConfig = allActionConfigs.get(ns).get(actionName);

                // extract Action Method
                String methodName = actionConfig.getMethodName();
                if (methodName == null || methodName.isEmpty()) {
                    methodName = ActionConfig.DEFAULT_METHOD;
                }

                // extract Action Class
                // use ObjectFactory to resolve real action class (particularly if using spring beans)
                Class resolvedActionClass = null;
                boolean actionClassNotFound = false;
                boolean actionInitializationError = false;
                String actionInitializationErrorMessage = "";
                if (objectFactory != null) {
                    try {
                        resolvedActionClass = objectFactory.getClassInstance(actionConfig.getClassName());
                        logger.debug("Resolved action class for {} : {}",
                                actionName, resolvedActionClass.getName());
                    } catch (ClassNotFoundException e) {
                        // class not in ClassLoader ?
                        actionClassNotFound = true;
                        logger.warn(e.toString());
                    }
                    catch (Exception e) {
                        actionInitializationError = true;
                        actionInitializationErrorMessage = e.getMessage();
                        logger.error(e.getMessage());
                    }
                }

                // extract Annotations
                Annotation[] methAnnotations = ScanReportUtils.extractMethodAnnotationsFromActionConfig(actionConfig, resolvedActionClass);
                List<Annotation> filteredMethAnnotations = new ArrayList<>();
                for (Annotation methAnnotation : methAnnotations) {
                    // filter annotation
                    if (this.annotationFilter.match(methAnnotation.annotationType().getName())) {
                        filteredMethAnnotations.add(methAnnotation);
                    }
                }

                ActionConfigData actionConfigData = new ActionConfigData.Builder(actionConfig)
                        .resolvedActionClass(resolvedActionClass)
                        .actionClassNotFound(actionClassNotFound)
                        .actionInitializationError(actionInitializationError)
                        .actionInitializationErrorMessage(actionInitializationErrorMessage)
                        .methodAnnotations(filteredMethAnnotations)
                        .build();
                tempStrActionConfigDataMap.put(actionName, actionConfigData);
            }
            actionConfigDataMap.put(ns, tempStrActionConfigDataMap);
        }

        return SUCCESS;
    }

    public static String annotationToHtmlWithWbr(String annotationString) {
        return HtmlUtils.insertWbrOnAnnotationString(annotationString);
    }

}
