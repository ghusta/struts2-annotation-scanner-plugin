package fr.husta.struts2.annotation_scanner.filter;

import java.util.Collection;
import java.util.Collections;
import java.util.regex.Pattern;

public class MultipleRegexPatternFilter implements Filter {

    private final Collection<Pattern> patterns;

    public MultipleRegexPatternFilter(Collection<Pattern> patterns) {
        if (patterns == null) {
            throw new IllegalArgumentException("Arg 'patterns' null");
        }
        this.patterns = Collections.unmodifiableCollection(patterns);
    }

    @Override
    public boolean match(String term) {
        if (term == null) {
            return false;
        }
        if (patterns.isEmpty()) {
            return true;
        }
        for (Pattern pattern : this.patterns) {
            if (RegexPatternFilter.EMPTY_PATTERN.equals(pattern.pattern())) {
                return true;
            }
            if (pattern.matcher(term).matches()) {
                return true;
            }
        }
        return false;
    }

}
