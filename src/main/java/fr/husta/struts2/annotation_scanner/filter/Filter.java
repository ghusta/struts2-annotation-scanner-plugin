package fr.husta.struts2.annotation_scanner.filter;

public interface Filter {

    boolean match(String term);

}
