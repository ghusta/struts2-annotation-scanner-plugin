package fr.husta.struts2.annotation_scanner.filter;

import java.util.regex.Pattern;

public class RegexPatternFilter implements Filter {

    static final String EMPTY_PATTERN = "";

    private final Pattern pattern;

    public RegexPatternFilter(Pattern pattern) {
        if (pattern == null) {
            throw new IllegalArgumentException("Arg 'pattern' null");
        }
        this.pattern = pattern;
    }

    @Override
    public boolean match(String term) {
        if (term == null) {
            return false;
        }
        if (EMPTY_PATTERN.equals(pattern.pattern())) {
            return true;
        }
        return this.pattern.matcher(term).matches();
    }

}
