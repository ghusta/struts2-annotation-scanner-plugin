package fr.husta.struts2.annotation_scanner;

import com.opensymphony.xwork2.config.entities.ActionConfig;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

public class ActionConfigData {

    protected ActionConfig actionConfig;

    protected Class<?> resolvedActionClass;

    protected boolean actionClassNotFound;

    protected boolean actionInitializationError;

    protected String actionInitializationErrorMessage = "";

    protected List<Annotation> methodAnnotations = new ArrayList<>();

    protected List<Annotation> classAnnotations = new ArrayList<>();

    protected ActionConfigData(ActionConfig orig) {
        this.actionConfig = orig;
    }

    protected ActionConfigData(String packageName, String actionName, String className) {
        this.actionConfig = new ActionConfig.Builder(packageName, actionName, className)
                .build();
    }

    protected ActionConfigData(String packageName, String actionName, String className, String methodName) {
        this.actionConfig = new ActionConfig.Builder(packageName, actionName, className)
                .methodName(methodName)
                .build();
    }

    public String getName() {
        return actionConfig.getName();
    }

    public String getClassName() {
        return actionConfig.getClassName();
    }

    public String getMethodName() {
        return actionConfig.getMethodName();
    }

    public String getPackageName() {
        return actionConfig.getPackageName();
    }

    public Class<?> getResolvedActionClass() {
        return resolvedActionClass;
    }

    public boolean isActionClassNotFound() {
        return actionClassNotFound;
    }

    public boolean isActionInitializationError() {
        return actionInitializationError;
    }

    public String getActionInitializationErrorMessage() {
        return actionInitializationErrorMessage;
    }

    public List<Annotation> getMethodAnnotations() {
        return methodAnnotations;
    }

    public List<Annotation> getClassAnnotations() {
        return classAnnotations;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", getName())
                .append("className", getClassName())
                .append("resolvedActionClass", resolvedActionClass)
                .append("actionClassNotFound", actionClassNotFound)
                .append("actionInitializationError", actionInitializationError)
                .append("actionInitializationErrorMessage", actionInitializationErrorMessage)
                .append("methodName", getMethodName())
                .append("packageName", getPackageName())
                .build();
    }

    public static class Builder {

        protected ActionConfigData target;

        public Builder(ActionConfig toClone) {
            if (toClone == null) {
                throw new IllegalArgumentException("Source ActionConfig null");
            }
            target = new ActionConfigData(toClone);
        }

        public Builder(String packageName, String actionName, String className, String methodName) {
            target = new ActionConfigData(packageName, actionName, className, methodName);
        }

        public Builder resolvedActionClass(Class<?> resolvedActionClass) {
            target.resolvedActionClass = resolvedActionClass;
            return this;
        }

        public Builder actionClassNotFound(boolean actionClassNotFound) {
            target.actionClassNotFound = actionClassNotFound;
            return this;
        }

        public Builder actionInitializationError(boolean actionInitializationError) {
            target.actionInitializationError = actionInitializationError;
            return this;
        }

        public Builder actionInitializationErrorMessage(String actionInitializationErrorMessage) {
            target.actionInitializationErrorMessage = actionInitializationErrorMessage;
            return this;
        }

        public Builder methodAnnotations(List<? extends Annotation> annotations) {
            target.methodAnnotations = new ArrayList<>(annotations);
            return this;
        }

        public Builder classAnnotations(List<Annotation> annotations) {
            target.classAnnotations = new ArrayList<>(annotations);
            return this;
        }

        public ActionConfigData build() {
            ActionConfigData res = target;
            return res;
        }

    }

}
