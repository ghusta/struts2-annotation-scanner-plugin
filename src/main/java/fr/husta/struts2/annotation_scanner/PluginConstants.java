package fr.husta.struts2.annotation_scanner;

/**
 * Struts constants for this plugin.
 * <br/>
 * See also Struts documentation :  <a href="https://struts.apache.org/core-developers/constant-configuration.html">Constant Configuration</a>.
 */
public class PluginConstants {

    /**
     * Filter for annotations to include.
     * Accepts comma-delimited patterns.
     */
    public static final String ANNOT_SCANNER_INCLUDED_ANNOTATIONS_PATTERNS = "annotation_scanner.included.annotations.patterns";

}
