package fr.husta.struts2.annotation_scanner.config;

import com.opensymphony.xwork2.inject.Inject;
import com.opensymphony.xwork2.util.TextParseUtil;
import fr.husta.struts2.annotation_scanner.PluginConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Plugin's constants parsing.
 *
 * @see com.opensymphony.xwork2.ognl.OgnlUtil
 */
public class PluginConfiguration {

    private static final Logger logger = LogManager.getLogger();

    private Set<Pattern> includedAnnotationsPatterns;

    /**
     * Comma-delimited patterns.
     *
     * @param commaDelimitedIncludedAnnotationsPatterns
     */
    @Inject(value = PluginConstants.ANNOT_SCANNER_INCLUDED_ANNOTATIONS_PATTERNS)
    public void setIncludedAnnotationsPatterns(String commaDelimitedIncludedAnnotationsPatterns) {
        logger.debug("Parsing constant : '{}' = [{}]",
                PluginConstants.ANNOT_SCANNER_INCLUDED_ANNOTATIONS_PATTERNS,
                commaDelimitedIncludedAnnotationsPatterns);
        Set<Pattern> localIncludedAnnotationsPatterns = new HashSet<>();

        if (StringUtils.isNotEmpty(commaDelimitedIncludedAnnotationsPatterns)) {
            localIncludedAnnotationsPatterns = parseIncludedAnnotationsPatterns(commaDelimitedIncludedAnnotationsPatterns);
        }
        this.includedAnnotationsPatterns = Collections.unmodifiableSet(localIncludedAnnotationsPatterns);
    }

    private Set<Pattern> parseIncludedAnnotationsPatterns(String commaDelimitedIncludedAnnotationsPatterns) {
        Set<String> localStringIncludedAnnotationsPatterns = TextParseUtil.commaDelimitedStringToSet(commaDelimitedIncludedAnnotationsPatterns);
        Set<Pattern> localIncludedAnnotationsPatterns = new HashSet<>();

        for (String pattern : localStringIncludedAnnotationsPatterns) {
            localIncludedAnnotationsPatterns.add(Pattern.compile(pattern));
        }

        return localIncludedAnnotationsPatterns;
    }

    public Set<Pattern> getIncludedAnnotationsPatterns() {
        return includedAnnotationsPatterns;
    }

}
