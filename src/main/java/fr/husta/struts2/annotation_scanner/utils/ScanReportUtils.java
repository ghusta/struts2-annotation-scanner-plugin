package fr.husta.struts2.annotation_scanner.utils;

import com.opensymphony.xwork2.config.entities.ActionConfig;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;

public class ScanReportUtils {

    private static final Logger logger = LogManager.getLogger();

    private ScanReportUtils() {
    }

    /**
     * @param actionConfig
     * @param resolvedActionClass className resolved as class
     * @return
     */
    public static Annotation[] extractMethodAnnotationsFromActionConfig(final ActionConfig actionConfig, Class<?> resolvedActionClass) {
        String packageName = actionConfig.getPackageName();
        String actionName = actionConfig.getName();
        String methodName = actionConfig.getMethodName();
        // String className = actionConfig.getClassName();

        if (methodName == null || methodName.isEmpty()) {
            methodName = ActionConfig.DEFAULT_METHOD;
        }

        Annotation[] foundAnnotationsOnMethod;
        Annotation[] foundAnnotationsOnEnclosingClass;

        if (resolvedActionClass == null) {
            return new Annotation[0];
        } else {
            AnnotatedElement annotatedMethod = MethodUtils.getMatchingMethod(resolvedActionClass, methodName);
            if (annotatedMethod == null) {
                return new Annotation[0];
            } else {
                foundAnnotationsOnMethod = AnnotationUtils.getAnnotations(annotatedMethod);

                foundAnnotationsOnEnclosingClass = AnnotationUtils.getAnnotations(resolvedActionClass);
                if (foundAnnotationsOnEnclosingClass.length != 0) {
                    logger.debug("Found {} annotation(s) on class '{}'",
                            foundAnnotationsOnEnclosingClass.length,
                            resolvedActionClass.getName());
                }
            }
        }
        return foundAnnotationsOnMethod;
    }

    /**
     * @param actionConfig
     * @return
     * @deprecated Use {@link #extractMethodAnnotationsFromActionConfig(ActionConfig, Class)}
     */
    @Deprecated
    public static Annotation[] extractMethodAnnotationsFromActionConfig(final ActionConfig actionConfig) {
        String packageName = actionConfig.getPackageName();
        String actionName = actionConfig.getName();
        String methodName = actionConfig.getMethodName();
        String className = actionConfig.getClassName();

        if (methodName == null || methodName.isEmpty()) {
            methodName = ActionConfig.DEFAULT_METHOD;
        }

        Annotation[] foundAnnotationsOnMethod = new Annotation[0];
        Annotation[] foundAnnotationsOnEnclosingClass;
        Class<?> actionEnclosingClass = null;
        if (className == null) {
            return new Annotation[0];
        }

        try {
            actionEnclosingClass = Class.forName(className);
        } catch (ClassNotFoundException e) {
            logger.trace("Class '{}' not found / resolved...", className);
        }
        if (actionEnclosingClass != null) {
            AnnotatedElement annotatedElement = MethodUtils.getMatchingMethod(actionEnclosingClass, methodName);
            foundAnnotationsOnMethod = AnnotationUtils.getAnnotations(annotatedElement);

            foundAnnotationsOnEnclosingClass = AnnotationUtils.getAnnotations(actionEnclosingClass);
            if (foundAnnotationsOnEnclosingClass.length != 0) {
                logger.debug("Found {} annotation(s) on class '{}'",
                        foundAnnotationsOnEnclosingClass.length,
                        actionEnclosingClass.getName());
            }
        }
        return foundAnnotationsOnMethod;
    }

}
