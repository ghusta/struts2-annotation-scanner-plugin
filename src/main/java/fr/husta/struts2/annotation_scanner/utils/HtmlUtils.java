package fr.husta.struts2.annotation_scanner.utils;

public abstract class HtmlUtils {

    /**
     * HTML &lt;wbr&gt; (word break).<br>
     * <a href="https://www.w3schools.com/TAgs/tag_wbr.asp">Reference</a>.
     *
     * @since HTML 5
     */
    public static final String HTML_TAG_WBR = "<wbr>";

    enum Position {
        BEFORE,
        AFTER;
    }

    /**
     * Insert a HTML tag &lt;wbr&gt;.
     *
     * @param phrase
     * @param token  where to insert the HTML tag
     * @return Result
     */
    public static String insertWbrAfter(final String phrase, final String token) {
        return internalInsertWbr(phrase, token, Position.AFTER);
    }

    /**
     * Insert a HTML tag &lt;wbr&gt;.
     *
     * @param phrase
     * @param token  where to insert the HTML tag
     * @return Result
     */
    public static String insertWbrBefore(final String phrase, final String token) {
        return internalInsertWbr(phrase, token, Position.BEFORE);
    }

    private static String internalInsertWbr(final String phrase, final String token, Position position) {
        if (position == Position.BEFORE) {
            return phrase.replace(token, HTML_TAG_WBR + token);
        } else if (position == Position.AFTER) {
            return phrase.replace(token, token + HTML_TAG_WBR);
        } else {
            throw new IllegalArgumentException("Position unknown");
        }
    }

    public static String insertWbrOnAnnotationString(final String annotationToString) {
        if (annotationToString == null) {
            return null;
        }

        // find first (
        int idxOpeningParenth = annotationToString.indexOf("(");
        String fqcn = annotationToString.substring(0, idxOpeningParenth);

        return insertWbrAfter(fqcn, ".") + annotationToString.substring(idxOpeningParenth);
    }

}
