package fr.husta.struts2.annotation_scanner.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class AnnotationUtils {

    private AnnotationUtils() {
    }

    public static <A extends Annotation> A findAnnotation(AnnotatedElement annotatedElement, Class<A> annotationType) {
        // TODO: use AnnotationUtils from other libs (spring, struts2, commons-lang3)
        throw new UnsupportedOperationException("NOT IMPLEMENTED");
    }

    /**
     * @param annotatedElement
     * @return
     * @see org.springframework.core.annotation.AnnotationUtils#getAnnotations(java.lang.reflect.AnnotatedElement)
     */
    public static Annotation[] getAnnotations(AnnotatedElement annotatedElement) {
        return annotatedElement.getAnnotations();
    }

    public static List<String> getAnnotationAttributesNames(final Class<? extends Annotation> annotationType) {
        List<String> res = new ArrayList<>();
        if (annotationType != null) {
            Method[] declaredMethods = annotationType.getDeclaredMethods();
            Arrays.sort(declaredMethods, new Comparator<Method>() {
                @Override
                public int compare(Method o1, Method o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
            for (Method method : declaredMethods) {
                res.add(method.getName());
            }
        }
        return res;
    }

    public static List<String> getAnnotationAttributesNames(final String annotationFqcn) throws ClassNotFoundException {
        Class<? extends Annotation> annotationClass = (Class<? extends Annotation>) Class.forName(annotationFqcn);
        return getAnnotationAttributesNames(annotationClass);
    }

    /**
     * Displays annotation FQCN + attributes.
     *
     * @param ann Annotation.
     * @return
     */
    public static String toString(final Annotation ann) {
        return org.apache.commons.lang3.AnnotationUtils.toString(ann);
    }

    /**
     * Displays annotation FQCN only.
     *
     * @param ann Annotation.
     * @return
     */
    public static String toStringClassName(final Annotation ann) {
        return "@" + ann.annotationType().getName();
    }

}
