package fr.husta.struts2.annotation_scanner.config;

import org.junit.Test;

import java.util.Set;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

public class PluginConfigurationTest {

    @Test
    public void getIncludedAnnotationsPatterns() {
        PluginConfiguration pluginConfiguration = new PluginConfiguration();

        Set<Pattern> includedAnnotationsPatterns;

        // test #1
        pluginConfiguration.setIncludedAnnotationsPatterns("");
        includedAnnotationsPatterns = pluginConfiguration.getIncludedAnnotationsPatterns();
        assertThat(includedAnnotationsPatterns).isEmpty();

        // test #2
        pluginConfiguration.setIncludedAnnotationsPatterns("javax.annotation.security.*");
        includedAnnotationsPatterns = pluginConfiguration.getIncludedAnnotationsPatterns();
        assertThat(includedAnnotationsPatterns).hasSize(1);

        // test #3
        pluginConfiguration.setIncludedAnnotationsPatterns("javax.annotation.security.RolesAllowed");
        includedAnnotationsPatterns = pluginConfiguration.getIncludedAnnotationsPatterns();
        assertThat(includedAnnotationsPatterns).hasSize(1);

        // test #4.a
        pluginConfiguration.setIncludedAnnotationsPatterns("javax.annotation.security.*, fr.husta.struts2.*");
        includedAnnotationsPatterns = pluginConfiguration.getIncludedAnnotationsPatterns();
        assertThat(includedAnnotationsPatterns).hasSize(2);

        // test #4.b
        pluginConfiguration.setIncludedAnnotationsPatterns("javax.annotation.security.*,\nfr.husta.struts2.*");
        includedAnnotationsPatterns = pluginConfiguration.getIncludedAnnotationsPatterns();
        assertThat(includedAnnotationsPatterns).hasSize(2);

        // test #5
        pluginConfiguration.setIncludedAnnotationsPatterns(null);
        includedAnnotationsPatterns = pluginConfiguration.getIncludedAnnotationsPatterns();
        assertThat(includedAnnotationsPatterns).isEmpty();
    }

    @Test
    public void checkRegexIncludedAnnotationsPatterns() {
        Pattern pattern1 = Pattern.compile("javax\\.annotation\\.security\\.(.*)");
        Pattern pattern2 = Pattern.compile("javax\\.annotation\\.security\\.RolesAllowed");

        String fqcn = "javax.annotation.security.RolesAllowed";

        // test #1.a
        assertThat(fqcn).matches(pattern1);

        // test #1.b
        assertThat("javaxAannotationBsecurityCZZZ").doesNotMatch(pattern1);

        // test #2.a
        assertThat(fqcn).matches(pattern2);

        // test #2.b
        assertThat("javaxAannotationBsecurityCRolesAllowed").doesNotMatch(pattern2);
    }

}