package fr.husta.struts2.annotation_scanner.filter;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

public class MultipleRegexPatternFilterTest {

    @Test
    public void testMatch() {
        Pattern pattern1 = Pattern.compile("javax\\.annotation\\.security\\.(.*)");
        Pattern pattern2 = Pattern.compile("javax\\.annotation\\.security\\.RolesAllowed");
        Collection<Pattern> patterns = Arrays.asList(pattern1, pattern2);
        MultipleRegexPatternFilter patternFilter = new MultipleRegexPatternFilter(patterns);

        String term;

        // test #1.a
        term = "javax.annotation.security.RolesAllowed";
        assertThat(patternFilter.match(term)).isTrue();

        // test #1.b
        term = "javax.annotation.security.DenyAll";
        assertThat(patternFilter.match(term)).isTrue();

        // test #2.a
        term = "fr.undefined.Bob";
        assertThat(patternFilter.match(term)).isFalse();

        // test #2.b
        term = "javaxXannotationYsecurity.RolesAllowed";
        assertThat(patternFilter.match(term)).isFalse();

        // test #3
        term = null;
        assertThat(patternFilter.match(term)).isFalse();

    }

    @Test
    public void testMatchLastPattern() {
        Pattern patternMoreSpecific = Pattern.compile("javax\\.annotation\\.security\\.RolesAllowed");
        Pattern patternLessSpecific = Pattern.compile("javax\\.annotation\\.security\\.(.*)");
        // pattern 2 first as it is the more specific
        List<Pattern> patterns = Arrays.asList(patternMoreSpecific, patternLessSpecific);
        MultipleRegexPatternFilter patternFilter = new MultipleRegexPatternFilter(patterns);

        String term;

        // test #1.a
        term = "javax.annotation.security.RolesAllowed";
        assertThat(patternFilter.match(term)).isTrue();

        // test #1.b
        term = "javax.annotation.security.DenyAll";
        assertThat(patternFilter.match(term)).isTrue();
    }

    @Test
    public void testMatchWithEmptyPatterns() {
        Collection<Pattern> patterns = Collections.emptyList();
        MultipleRegexPatternFilter patternFilter = new MultipleRegexPatternFilter(patterns);

        String term;

        term = "org.apache.struts2.interceptor.validation.SkipValidation";
        assertThat(patternFilter.match(term)).as("Should match as empty patterns means accept all").isTrue();
    }

    @Test
    public void testMatchWithEmptyStringPattern() {
        List<Pattern> patterns = Collections.singletonList(Pattern.compile(""));
        MultipleRegexPatternFilter patternFilter = new MultipleRegexPatternFilter(patterns);

        String term;

        term = "org.apache.struts2.interceptor.validation.SkipValidation";
        assertThat(patternFilter.match(term)).as("Should match as empty patterns means accept all").isTrue();
    }

}