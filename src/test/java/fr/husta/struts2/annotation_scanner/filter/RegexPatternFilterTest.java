package fr.husta.struts2.annotation_scanner.filter;

import org.junit.Test;

import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

public class RegexPatternFilterTest {

    @Test
    public void testMatchEmptyPattern() {
        Pattern pattern = Pattern.compile("");
        RegexPatternFilter patternFilter = new RegexPatternFilter(pattern);

        String term;

        // test #1
        term = "javax.annotation.security.RolesAllowed";
        assertThat(patternFilter.match(term)).isTrue();

    }

}