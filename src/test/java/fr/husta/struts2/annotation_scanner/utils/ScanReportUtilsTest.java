package fr.husta.struts2.annotation_scanner.utils;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.config.entities.ActionConfig;
import com.opensymphony.xwork2.config.entities.ResultConfig;
import org.junit.Test;

import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import java.lang.annotation.Annotation;

import static org.assertj.core.api.Assertions.assertThat;

public class ScanReportUtilsTest {

    private static class InnerTestAction extends ActionSupport {

        @Override
        @RolesAllowed({"ADMIN", "USER"})
        public String execute() throws Exception {
            return SUCCESS;
        }

        @Override
        @DenyAll
        public String input() throws Exception {
            return INPUT;
        }
    }

    @Test
    public void extractMethodAnnotationsFromActionConfig_defaultMethod_OK() {
        ActionConfig actionConfig;
        Annotation[] annotations;

        actionConfig = new ActionConfig.Builder("test", "my-inner-action", InnerTestAction.class.getName())
                .addResultConfig(new ResultConfig.Builder("aaa", "bbb").build())
                .build();

        annotations = ScanReportUtils.extractMethodAnnotationsFromActionConfig(actionConfig);
        assertThat(annotations)
                .hasSize(1)
                .hasOnlyElementsOfType(RolesAllowed.class);
    }

    @Test
    public void extractMethodAnnotationsFromActionConfig_explicitMethod_OK() {
        ActionConfig actionConfig;
        Annotation[] annotations;

        actionConfig = new ActionConfig.Builder("test", "my-inner-action2", InnerTestAction.class.getName())
                .methodName("execute")
                .addResultConfig(new ResultConfig.Builder("aaa", "bbb").build())
                .build();

        annotations = ScanReportUtils.extractMethodAnnotationsFromActionConfig(actionConfig);
        assertThat(annotations)
                .hasSize(1)
                .hasOnlyElementsOfType(RolesAllowed.class);
    }

    @Test
    public void extractMethodAnnotationsFromActionConfig_explicitMethod2_OK() {
        ActionConfig actionConfig;
        Annotation[] annotations;

        actionConfig = new ActionConfig.Builder("test", "my-inner-action3", InnerTestAction.class.getName())
                .methodName("input")
                .addResultConfig(new ResultConfig.Builder("aaa", "bbb").build())
                .build();

        annotations = ScanReportUtils.extractMethodAnnotationsFromActionConfig(actionConfig);
        assertThat(annotations)
                .hasSize(1)
                .hasOnlyElementsOfType(DenyAll.class);
    }

    @Test
    public void extractMethodAnnotationsFromActionConfig_methodNameUnknown() {
        ActionConfig actionConfig;
        Annotation[] annotations;

        actionConfig = new ActionConfig.Builder("test", "my-inner-action4", InnerTestAction.class.getName())
                .methodName("{1}")
                .addResultConfig(new ResultConfig.Builder("aaa", "bbb").build())
                .build();

        annotations = ScanReportUtils.extractMethodAnnotationsFromActionConfig(actionConfig, InnerTestAction.class);
        assertThat(annotations).isEmpty();
    }

    @Test
    public void extractMethodAnnotationsFromActionConfig_classNull() {
        ActionConfig actionConfig;
        Annotation[] annotations;

        actionConfig = new ActionConfig.Builder("test", "my-action", null).build();

        annotations = ScanReportUtils.extractMethodAnnotationsFromActionConfig(actionConfig);
        assertThat(annotations).isEmpty();
    }

    @Test
    public void extractMethodAnnotationsFromActionConfig_classUnknown() {
        ActionConfig actionConfig;
        Annotation[] annotations;

        actionConfig = new ActionConfig.Builder("test", "my-action", "undefined").build();

        annotations = ScanReportUtils.extractMethodAnnotationsFromActionConfig(actionConfig);
        assertThat(annotations).isEmpty();
    }

}