package fr.husta.struts2.annotation_scanner.utils;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.junit.Test;

import javax.annotation.Generated;
import javax.annotation.security.RolesAllowed;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.List;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.assertj.core.api.Assertions.assertThat;

public class AnnotationUtilsTest {

    @RolesAllowed({"ADMIN", "USER"})
    @Generated(value = {"TEST - not scanned (@Retention = SOURCE)"})
    private class InnerTest {

    }

    /**
     * Annotations on methods.
     */
    private class Inner2Test {

        @RolesAllowed({"AAA"})
        private void methA() {
        }

        @RolesAllowed({"BBB"})
        public void methB() {
        }

        public boolean methC() {
            return false;
        }

        @Foo(value = "Hi", name = "???", count = 123)
        public void methD() {
        }

    }

    /**
     * Custom annotation
     */
    @Retention(RUNTIME)
    @Target({TYPE, ANNOTATION_TYPE, METHOD})
    public @interface Foo {
        String value() default "Hello";

        String name();

        int count();
    }

    @Test
    public void test_getAnnotations() {
        AnnotatedElement annotatedElement = null;
        Annotation[] foundAnnotations;

        annotatedElement = MethodUtils.getMatchingMethod(Inner2Test.class, "methB");
        assertThat(annotatedElement).isNotNull();
        foundAnnotations = AnnotationUtils.getAnnotations(annotatedElement);
        assertThat(foundAnnotations).hasSize(1);

        annotatedElement = MethodUtils.getMatchingMethod(Inner2Test.class, "methC");
        assertThat(annotatedElement).isNotNull();
        foundAnnotations = AnnotationUtils.getAnnotations(annotatedElement);
        assertThat(foundAnnotations).isEmpty();

        annotatedElement = InnerTest.class;
        assertThat(annotatedElement).isNotNull();
        foundAnnotations = AnnotationUtils.getAnnotations(annotatedElement);
        assertThat(foundAnnotations).as("Annotations with retention = SOURCE are not retained at compile time")
                .hasSize(1);
    }

    @Test
    public void test_getAnnotation_multipleParameters() {
        AnnotatedElement annotatedElement = null;
        Annotation[] foundAnnotations;

        annotatedElement = MethodUtils.getMatchingMethod(Inner2Test.class, "methD");
        assertThat(annotatedElement).isNotNull();
        foundAnnotations = AnnotationUtils.getAnnotations(annotatedElement);
        assertThat(foundAnnotations).hasSize(1);

        Annotation firstAnnotation = foundAnnotations[0];
        int nbAttributes = firstAnnotation.annotationType().getDeclaredMethods().length;
        assertThat(nbAttributes).isEqualTo(3);
    }

    @Test
    public void test_getAnnotationAttributesNames() {
        List<String> attributesNames;

        attributesNames = AnnotationUtils.getAnnotationAttributesNames(RolesAllowed.class);
        assertThat(attributesNames).containsExactly("value");

        // names are sorted
        attributesNames = AnnotationUtils.getAnnotationAttributesNames(Generated.class);
        assertThat(attributesNames).containsExactly("comments", "date", "value");

        attributesNames = AnnotationUtils.getAnnotationAttributesNames(Override.class);
        assertThat(attributesNames.size()).isZero();
    }

    @Test
    public void test_getAnnotationAttributesNamesFromConventionPlugin() throws ClassNotFoundException {
        List<String> attributesNames;

        // names are sorted
        attributesNames = AnnotationUtils.getAnnotationAttributesNames("org.apache.struts2.convention.annotation.Action");
        assertThat(attributesNames).containsExactly("className", "exceptionMappings", "interceptorRefs", "params", "results", "value");
    }

    @Test
    public void test_toString() {
        Annotation[] annotations = InnerTest.class.getAnnotations();
        assertThat(annotations).isNotEmpty();

        System.out.println(annotations[0]);
        assertThat(AnnotationUtils.toString(annotations[0])).isEqualTo("@javax.annotation.security.RolesAllowed(value=[ADMIN,USER])");
        assertThat(AnnotationUtils.toStringClassName(annotations[0])).isEqualTo("@javax.annotation.security.RolesAllowed");
    }

    @Test
    public void test_annotationsOnMethods() {
        Method[] declaredMethods = Inner2Test.class.getDeclaredMethods();
        int annotCount = 0;
        for (Method method : declaredMethods) {
            Annotation[] annotations = method.getAnnotations();
            annotCount += annotations.length;
            for (Annotation annotation : annotations) {
                System.out.println(String.format("on method '%s' : found %s",
                        method.getName(),
                        AnnotationUtils.toString(annotations[0])));
            }
        }
        assertThat(annotCount).isGreaterThan(0);
    }

}