package fr.husta.struts2.annotation_scanner.utils;

import org.junit.Test;

import static fr.husta.struts2.annotation_scanner.utils.HtmlUtils.HTML_TAG_WBR;
import static org.assertj.core.api.Assertions.assertThat;

public class HtmlUtilsTest {

    @Test
    public void testInsertWbrAfter() {
        String htmlInput = "This.Is.Great";
        String res = HtmlUtils.insertWbrAfter(htmlInput, ".");

        assertThat(res).isEqualTo("This." + HTML_TAG_WBR + "Is." + HTML_TAG_WBR + "Great");
    }

    @Test
    public void testInsertWbrOnAnnotation() {
        String annotationToString = "@org.springframework.security.access.prepost.PreAuthorize(value=hasRole('SUPERUSER') && isAuthenticated())";
        String res = HtmlUtils.insertWbrOnAnnotationString(annotationToString);

        assertThat(res).isEqualTo("@org." + HTML_TAG_WBR + "springframework." + HTML_TAG_WBR + "security."
                + HTML_TAG_WBR + "access." + HTML_TAG_WBR + "prepost." + HTML_TAG_WBR
                + "PreAuthorize(value=hasRole('SUPERUSER') && isAuthenticated())");
    }

}